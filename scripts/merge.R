#######################
# Merge rCOGS Results #
#######################

library(data.table)
library(dplyr)

args <- commandArgs(trailingOnly = T)

#args <- "/Users/s2078878/ownCloud/rotation_two/nextflow/results/COVID-19_(critical_illness_vs_population)_growingSpecific.txt"

out <- data.frame(ensg = "REMOVE")
for(i in args){
   
   single <- fread(i)
   out <- merge(out, single, by = "ensg", all = T)

}

out <- filter(out, ensg != "REMOVE")

test <- tail(strsplit(strsplit(basename(args[1]), "[.]")[[1]][1], "_")[[1]],1)
fwrite(out, paste0("rCOGS_", test, ".txt"), sep = "\t", quote = F, row.names = F)
