#!/usr/bin/env nextflow

/*
 * pipeline input parameters
 */
params.parameters = "$baseDir/exampleParams.csv"
params.outdir = "$baseDir"
params.file = false

log.info """\
         =======================================================================
         B L O C K S H I F T E R   -   R C O G S   -   G W A S   P I P E L I N E
         =======================================================================
         parameters_file:   ${params.parameters}
         out_directory:     ${params.outdir}
         =======================================================================
         """
         .stripIndent()

/*
* Add bam files to a channel and get bam and index file pairs
*/
Channel
    .fromPath(params.parameters)
    .splitCsv(header:true)
    .map{ row -> tuple(row.row, row.ftp_link, row.trait, row.chr_id, row.pos_id, row.p_id, row.design, row.cases, row.controls)}
    .set{ parameters_ch }

(ftp_ch, file_ch) = ( params.file
                 ? [Channel.empty(), parameters_ch]
                 : [parameters_ch, Channel.empty()] )

/*
* Load in support files
*/
maf = file("$baseDir/data/support/uk10k_reference_maf.tab")
ld = file("$baseDir/data/support/hapmap_1cM_recomb.bed")
csnps = file("$baseDir/data/support/coding_snps_vep_ensembl75.tab")

/*
* Load in pchic data
*/
features = file("$baseDir/data/pchic/peakMatrixFormatted.txt.gz")
digest = file("$baseDir/data/pchic//harmonisedDigest.txt.gz")
baits = file("$baseDir/data/pchic//harmonisedBaits.txt.gz")


/*
 * Download GWAS data 
 */
process download {
    tag "Downloading GWAS Summary Statistics"

    input:
    set row, ftp_link, trait, chr_id, pos_id, p_id, design, cases, controls from ftp_ch
    
    output:
    set row, path("*"), trait, chr_id, pos_id, p_id, design, cases, controls into optional_ch

    script:
    """
    wget ${ftp_link}

    NAME=\$(basename ${ftp_link})
    EXTENSION="\${NAME##*.}"

    if [[ "\$EXTENSION" == "bgz" ]]
    then
        
        NEW_NAME=\$(basename -s .bgz ${ftp_link})

        gunzip -c \${NAME} > \${NEW_NAME}

        rm \${NAME}

    fi
    """
}

/*
 * Format GWAS data
 */
process format {
    tag "Formatting GWAS Summary Statistics"

    input:
    set row, path(file), trait, chr_id, pos_id, p_id, design, cases, controls from file_ch.mix(optional_ch)

    output:
    set path("${trait}_formatted.txt.gz"), trait, design, cases, controls into formatted_ch

    script:
    """
    cp $baseDir/scripts/fixFormat.R .
    Rscript fixFormat.R "${trait}" "${file}" "${chr_id}" "${pos_id}" "${p_id}"
    """
}

/*
 * run rCOGS - this will run run both the overall and cell type specific analyses. 
 * This process will also output the ppi files needed for blockshifter.
 */
process rcogs {
    tag "Running rCOGS"

    input:
    set path(formatted), trait, design, cases, controls from formatted_ch

    output:
    set trait, path("${trait}.ppi.gz") into ppi_ch
    path("${trait}_growingSpecific.txt") into growingSpecific_ch
    path("${trait}_quiescentSpecific.txt") into quiescentSpecific_ch
    path("${trait}_senescentSpecific.txt") into senescentSpecific_ch
    path("${trait}_overall.txt") into overallSpecific_ch
    path("${trait}_checkfile.txt") into checkfile_ch

    script:
    """
    cp $baseDir/scripts/cogsSNP.R .
    Rscript cogsSNP.R "${trait}" "${formatted}" "${design}" "${cases}" "${controls}" "${maf}" "${ld}" "${features}" "${csnps}" "${digest}" "${baits}"
    """
}

/*
 * Merge all of the rCOGS outputs for cell-state specific and overall
 */
process merge_rcogs {
    tag "Merging rCOGS results"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(growing) from growingSpecific_ch.collect()
    path(quiescent) from quiescentSpecific_ch.collect()
    path(senescent) from senescentSpecific_ch.collect()
    path(overall) from overallSpecific_ch.collect()
 
    output:
    set path("rCOGS_growingSpecific.txt"), path("rCOGS_quiescentSpecific.txt"), path("rCOGS_senescentSpecific.txt") into results_ch

    script:
    """
    cp $baseDir/scripts/merge.R .

    Rscript merge.R ${growing}
    Rscript merge.R ${quiescent} 
    Rscript merge.R ${senescent}
    Rscript merge.R ${overall}
    """
}

/*
 * Merge all of the rCOGS outputs for cell-state specific and overall
 */
process checkfile {
    tag "Creating Checkfile"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(checkfile) from checkfile_ch.collect()

    output:
    path("checkfile.txt") into checkfile_out_ch

    script:
    """
    cp $baseDir/scripts/bind.R .
    Rscript bind.R ${checkfile}
    """
}

/*
 * Run blockshifter for each of the combinations
 */
process blockshifter {
    tag "Running Blockshifter"

    input:
    set trait, path(ppi) from ppi_ch

    output:
    path("${trait}_groVsQui.txt") into groVsQui_ch
    path("${trait}_groVsSen.txt") into groVsSen_ch
    path("${trait}_quiVsSen.txt") into quiVsSen_ch

    script:
    """
    cp $baseDir/scripts/blockshifter.R $baseDir/scripts/common.R .

    Rscript blockshifter.R \
        --contacts_file=${features} \
        --pmi_file=${ppi} \
        --perm_no=10000 \
        --test_tissue=Qui.Rep1,Qui.Rep2 \
        --control_tissue=Growing.Rep1,Growing.Rep2 \
        --output_file="${trait}_groVsQui.txt" \
        --metric=ppi \
        --all_vs_all=0 \
        --test_tissue_label=Quiescent \
        --control_tissue_label=Growing

    Rscript blockshifter.R \
        --contacts_file=${features} \
        --pmi_file=${ppi} \
        --perm_no=10000 \
        --test_tissue=Sen.Rep1,Sen.Rep2 \
        --control_tissue=Growing.Rep1,Growing.Rep2 \
        --output_file="${trait}_groVsSen.txt" \
        --metric=ppi \
        --all_vs_all=0 \
        --test_tissue_label=Senescent \
        --control_tissue_label=Growing

    Rscript blockshifter.R \
        --contacts_file=${features} \
        --pmi_file=${ppi} \
        --perm_no=10000 \
        --test_tissue=Sen.Rep1,Sen.Rep2 \
        --control_tissue=Qui.Rep1,Qui.Rep2 \
        --output_file="${trait}_quiVsSen.txt" \
        --metric=ppi \
        --all_vs_all=0 \
        --test_tissue_label=Senescent \
        --control_tissue_label=Quiescent
    """
}

/*
 * Merge all of the blockshifter output for each comparison
 */
process merge_blockshifter {
    tag "Merging blockshifter results"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(gro_qui) from groVsQui_ch.collect()
    path(gro_sen) from groVsSen_ch.collect()
    path(qui_sen) from quiVsSen_ch.collect()

    output:
    set path("blockshifter_groVsQui.txt"), path("blockshifter_groVsSen.txt"), path("blockshifter_quiVsSen.txt") into blockshifter_results_ch

    script:
    """
    cp $baseDir/scripts/bind.R .

    Rscript bind.R ${gro_qui}
    Rscript bind.R ${gro_sen}
    Rscript bind.R ${qui_sen}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All GWAS studies analysed.\n" : "\nOops .. something went wrong.\n" )
}