library(data.table)
library(dplyr)
library(pheatmap)
library(annotables)
library(clipr)
library(stringr)
library(tidyr)

grow <- fread("/Users/s2078878/ownCloud/rotation_two/main/results/rCOGS/growingSpecific.txt", quote = "")
qui <- fread("/Users/s2078878/ownCloud/rotation_two/main/results/rCOGS/quiescentSpecific.txt", quote = "")
sen <- fread("/Users/s2078878/ownCloud/rotation_two/main/results/rCOGS/senescentSpecific.txt", quote = "")

grow <- as.data.frame(grow)
qui <- as.data.frame(qui)
sen <- as.data.frame(sen)

grow_new <- data.frame(ensgene = c(""))
for(i in names(grow)[endsWith(names(grow), "_SNPs")]){
  
  ## select the columns needed and rename if necessary 
  cogs <- str_replace(i, "SNPs", "cogs")
  int <- grow %>% dplyr::select("ensg", i, cogs)
  colnames(int)[2] <- "SNPs"
  colnames(int)[3] <- "cogs"
  
  ## Filter the cogs scores 
  int <- filter(int, cogs > 0.5)
  
  ## unnest the SNPs 
  int <- int %>% mutate(pid = strsplit(as.character(int$SNPs), ", ")) %>% 
    unnest(pid) %>% 
    select("ensg", "pid", "cogs")
  
  if(length(int$ensg) != 0) {
    
    setDT(int)[,c('SNP_chr','SNP_pos','perc'):=tstrsplit(pid,":")]
    
    int <- int %>% group_by(ensg) %>% 
      top_n(1, as.numeric(perc)) 
    
    colnames(int)[1] <- "ensgene"
    int <- left_join(int, grch37, by = "ensgene")
    int <- filter(int, chr == SNP_chr & biotype == "protein_coding")
    
    int <- int %>% select("ensgene", "pid", "cogs")
    
    ## rename back the columns ready to merge
    colnames(int)[2] <- i
    colnames(int)[3] <- cogs
    
    grow_new <- full_join(grow_new, int, by = "ensgene")
    
  } else {
    
    print(paste0("SKIPPING: ", i))
    
  }
}


qui_new <- data.frame(ensgene = c(""))
for(i in names(qui)[endsWith(names(qui), "_SNPs")]){
  
  ## select the columns needed and rename if necessary 
  cogs <- str_replace(i, "SNPs", "cogs")
  int <- qui %>% dplyr::select("ensg", i, cogs)
  colnames(int)[2] <- "SNPs"
  colnames(int)[3] <- "cogs"
  
  ## Filter the cogs scores 
  int <- filter(int, cogs > 0.5)
  
  ## unnest the SNPs 
  int <- int %>% mutate(pid = strsplit(as.character(int$SNPs), ", ")) %>% 
    unnest(pid) %>% 
    select("ensg", "pid", "cogs")
  
  if(length(int$ensg) != 0) {
    
    setDT(int)[,c('SNP_chr','SNP_pos','perc'):=tstrsplit(pid,":")]
    
    int <- int %>% group_by(ensg) %>% 
      top_n(1, as.numeric(perc)) 
    
    colnames(int)[1] <- "ensgene"
    int <- left_join(int, grch37, by = "ensgene")
    int <- filter(int, chr == SNP_chr & biotype == "protein_coding")
    
    int <- int %>% select("ensgene", "pid", "cogs")
    
    ## rename back the columns ready to merge
    colnames(int)[2] <- i
    colnames(int)[3] <- cogs
    
    qui_new <- full_join(qui_new, int, by = "ensgene")
    
  } else {
    
    print(paste0("SKIPPING: ", i))
    
  }
}


sen_new <- data.frame(ensgene = c(""))
for(i in names(sen)[endsWith(names(sen), "_SNPs")]){
  
  ## select the columns needed and rename if necessary 
  cogs <- str_replace(i, "SNPs", "cogs")
  int <- sen %>% dplyr::select("ensg", i, cogs)
  colnames(int)[2] <- "SNPs"
  colnames(int)[3] <- "cogs"
  
  ## Filter the cogs scores 
  int <- filter(int, cogs > 0.5)
  
  ## unnest the SNPs 
  int <- int %>% mutate(pid = strsplit(as.character(int$SNPs), ", ")) %>% 
    unnest(pid) %>% 
    select("ensg", "pid", "cogs")
  
  if(length(int$ensg) != 0) {
    
    setDT(int)[,c('SNP_chr','SNP_pos','perc'):=tstrsplit(pid,":")]
    
    int <- int %>% group_by(ensg) %>% 
      top_n(1, as.numeric(perc)) 
    
    colnames(int)[1] <- "ensgene"
    int <- left_join(int, grch37, by = "ensgene")
    int <- filter(int, chr == SNP_chr & biotype == "protein_coding")
    
    int <- int %>% select("ensgene", "pid", "cogs")
    
    ## rename back the columns ready to merge
    colnames(int)[2] <- i
    colnames(int)[3] <- cogs
    
    sen_new <- full_join(sen_new, int, by = "ensgene")
    
  } else {
    
    print(paste0("SKIPPING: ", i))
    
  }
}

grow <- grow_new
qui <- qui_new
sen <- sen_new

colnames(grow)[1] <- "ensgene"
colnames(qui)[1] <- "ensgene"
colnames(sen)[1] <- "ensgene"

grow <- left_join(grow, grch37, by = "ensgene")
qui <- left_join(qui, grch37, by = "ensgene")
sen <- left_join(sen, grch37, by = "ensgene")

grow <- grow[!duplicated(grow$ensgene),]
qui <- qui[!duplicated(qui$ensgene),]
sen <- sen[!duplicated(sen$ensgene),]

grow <- filter(grow, biotype == "protein_coding")
qui <- filter(qui, biotype == "protein_coding")
sen <- filter(sen, biotype == "protein_coding")

grow <- dplyr::select(grow, c("ensgene",ends_with("_cogs")))
qui <- dplyr::select(qui, c("ensgene",ends_with("_cogs")))
sen <- dplyr::select(sen, c("ensgene",ends_with("_cogs")))

total_grow <- grow %>% filter_at(vars(-ensgene), any_vars(. > 0.5))
total_qui <- qui %>% filter_at(vars(-ensgene), any_vars(. > 0.5))
total_sen <- sen %>% filter_at(vars(-ensgene), any_vars(. > 0.5))

length(unique(c(total_grow$ensgene, total_qui$ensgene, total_sen$ensgene)))

grow <- grow[,-1]
qui <- qui[,-1]
sen <- sen[,-1]

grow <- as.data.frame(grow)
qui <- as.data.frame(qui)
sen <- as.data.frame(sen)

grow_names <- names(grow) %>% str_replace_all("_"," ") %>% str_replace_all("cogs","")
qui_names <- names(qui) %>% str_replace_all("_"," ") %>% str_replace_all("cogs","")
sen_names <- names(sen) %>% str_replace_all("_"," ") %>% str_replace_all("cogs","")


###############
# Get numbers #
###############
grow_numbers <- c()
for(i in names(grow)){
  num <- sum(grow[, i] > 0.5, na.rm = T)
  grow_numbers <- c(grow_numbers, num)
}

qui_numbers <- c()
for(i in names(qui)){
  num <- sum(qui[, i] > 0.5, na.rm = T)
  qui_numbers <- c(qui_numbers, num)
}

sen_numbers <- c()
for(i in names(sen)){
  num <- sum(sen[, i] > 0.5, na.rm = T)
  sen_numbers <- c(sen_numbers, num)
}

sum(c(grow_numbers, qui_numbers, sen_numbers))

###################
# Make dataframes #
###################
grow_df <- data.frame(name = grow_names, values = grow_numbers)
qui_df <- data.frame(name = qui_names, values = qui_numbers)
sen_df <- data.frame(name = sen_names, values = sen_numbers)

dim(grow_df)
dim(qui_df)
dim(sen_df)

combined <- merge(grow_df, qui_df, by="name")
combined <- merge(combined, sen_df, by="name")


############################
# Prepare data for heatmap #
############################
combined <- as.data.frame(combined)
row.names(combined) <- combined$name
combined <- dplyr::select(combined, -name)
colnames(combined) <- c("Growing", "Quiescent", "Senescent")

combined <- filter(combined, Growing != 0.0 & Quiescent != 0.0 & Senescent != 0.0)
combined <- filter(combined, Growing != "" & Quiescent != "" & Senescent != "")

matrix <- data.matrix(combined)

matrix <- t(matrix)

pheatmap(mat = log10(matrix + 1),
         clustering_method = 'ward.D2',
         legend = FALSE,
         display_numbers = matrix,
         cellheight = 16,
         cellwidth = 16,
         angle_col = "45",
         cluster_rows = F)
