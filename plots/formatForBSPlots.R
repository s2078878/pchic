library(data.table)

gro_sen <- fread("/Users/s2078878/ownCloud/rotation_two/nextflow/data/blockshifter_groVsSen.txt")
gro_qui <- fread("/Users/s2078878/ownCloud/rotation_two/nextflow/data/blockshifter_groVsQui.txt")
qui_sen <- fread("/Users/s2078878/ownCloud/rotation_two/nextflow/data/blockshifter_quiVsSen.txt")

check <- fread("/Users/s2078878/ownCloud/rotation_two/nextflow/data/checkfile.txt")

colnames(gro_sen)[3:10] <- paste0("g_s.", colnames(gro_sen)[3:10])
colnames(gro_qui)[3:10] <- paste0("g_q.", colnames(gro_qui)[3:10])
colnames(qui_sen)[3:10] <- paste0("q_s.", colnames(qui_sen)[3:10])

merged <- merge(gro_sen, gro_qui, by = c("type", 'gwas'))
merged <- merge(merged, qui_sen, by = c("type", 'gwas'))
merged$gwas <- gsub(".ppi.gz", "", merged$gwas)

merged <- merge(merged, check, by.x = "gwas", by.y = "task_id")

colnames(merged)[1] <- "trait"

fwrite(merged, "/Users/s2078878/ownCloud/rotation_two/nextflow/data/TEST.txt", sep = "\t", quote = F, row.names = F)
