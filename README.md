# GWAS Pipeline for rCOGS and Blockshifter

This Nextflow pipeline will run both rCOGS and blockshifter on a set of GWAS summary statistics provided in a parameters file. 

## Running the Pipeline

### 1. Log into Eddie
Log into eddie and ssh into a wild-west node

```shell
ssh node2c16
```

### 2. Clone this Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/pchic.git
cd pchic
```

### 3.Create Environment

To create a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
```

Then activate the environment
```shell
conda activate pchic
```

You will now have all of the necessary packages with the exception of rCOGS itself. To download rCOGS, run the following commands:

```shell
R # open R
library(devtools) # load devtools library
install_github("ollyburren/rCOGS") #download rCOGS
q() # quit R
```

### 4.Make Support Files

The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names. An example parameters file is also provided.

| row | ftp_link                                   | trait         | chr_id     | pos_id   | p_id    | design | cases | controls |
|-----|--------------------------------------------|---------------|------------|----------|---------|--------|-------|----------|
| 1   | ftp/link/to/summary/statistics/file.csv    | COVID-19      | chr        | base     | p       | CC     | 2657  | 10298    |
| 2   | ftp/link/to/summary/statistics/file.tsv    | obesity       | chrom      | pos      | p-value | QUANT  | 6583  | NA       |
| 3   | ftp/link/to/summary/statistics/file.txt    | breast_cancer | chromosome | Position | p_value | QUANT  | 7584  | NA       |
| 4   | ftp/link/to/summary/statistics/file.tsv.gz | anaemia       | Chr        | loc      | pValue  | CC     | 5284  | 7297     |

#### Support file columns:
**row** - Row number.

**ftp_link** - FTP links for GWAS summary statistics. These can be downloaded from websites such as [GWAS Catalog](https://www.ebi.ac.uk/gwas/) and [GWAS Atlas](https://atlas.ctglab.nl/). Summary statistics can be a range of file types e.g. .csv, .tsv, .txt, etc. Also note that there is no need to uncompress gzipped files before being used in the pipeline (row 4). 

**trait** - This should be a unique ID for the trait being tested. Try to avoid spaces in this unique ID.

Due to there being no standard format for GWAS summary statistics, reformatting of these files needs to take place so that they will be accepted by rCOGs and blockshifter. The Nextflow pipeline will automatically reformat the files, however necessary column names need to be provided for this take place. These are:

**chr_id** - In the GWAS summary statistics, what column name is used for the chromosome column?

**pos_id** - In the GWAS summary statistics, what column name is used for the position of each mutation?

**p_id** - In the GWAS summary statistics, what column name is used for p-values?

Additional statistics from each GWAS study also need to be provided, these are:

**design** - Either CC for case-control, or QUANT for quantitative study.

**cases** - Number of cases used in the study, if a quantitative study, this will be the total number of people used in the study. 

**controls** - Number of controls, if a quantitative design (i.e. design set to "QUANT") set  controls to "NA". 


### 5.Run Pipeline

Everything should now be ready to run the pipeline on the desired GWAS summary files. A typical command will look something similar to the one below, substituting in the path to your own parameter file. If the --parameters option is left empty, the example parameters file will be used. If the --outdir option is left empty, the run directory will be used. Add --file if the files have been predownloaded, if this flag is used the parameters file should remain the same, however instead of providing an ftp_link the file paths should be provided.

```shell
nextflow run main.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 2)
  --outdir path/to/out/directory/ # where should the results folder be created?
  --file # add this flag if files are already downloaded and are not to be downloaded from ftp link.
```

This will output a results directory containing rCOGS and Blockshifter results. rCOGS results will be split into growing, quiescent, and senescent specific gene prioritisations, as well as an overall file. Blockshifter results will be split into the three main comparisons growing vs quiescent, growing vs senescent, and quiescent vs senescent. 









